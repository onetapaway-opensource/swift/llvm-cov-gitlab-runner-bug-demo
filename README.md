# llvm-cov-gitlab-runner-bug-demo

This is a demo project for the sole purpose of reproducing a bug with running `llvm-cov` from a `gitlab-runner` on a Mac. The name is atrocious, but at least it's descriptive 😉



To prove that it's working locally, I wrote a script that mimics what gitlab-runner does.
```
./mimic-gitlab-ci.sh
```

Then simply execute the gitlab-runner with a shell executor to see that it doesn't work there.
```
gitlab-runner exec shell run_tests
```


You'll see a populated coverage report when you run `mimic-gitlab-ci.sh`, and you will see an empty coverage report when you run via `gitlab-runner`.



Here is the output when executing via gitlab-runner

```
Filename                      Regions    Missed Regions     Cover   Functions  Missed Functions  Executed       Lines      Missed Lines     Cover
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
TOTAL                               0                 0         -           0                 0         -           0                 0         -
```




And here is the output when executing `mimic-gitlab-ci.sh`

```
Filename                                                                                                          Regions    Missed Regions     Cover   Functions  Missed Functions  Executed       Lines      Missed Lines     Cover
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
llvm-cov-gitlab-runner-bug-demo/Sources/llvm-cov-gitlab-runner-bug-demo/llvm_cov_gitlab_runner_bug_demo.swift           2                 1    50.00%           2                 1    50.00%           9                 6    33.33%
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
TOTAL                                                                                                                   2                 1    50.00%           2                 1    50.00%           9                 6    33.33%

```




Hope this helps! 🙂
