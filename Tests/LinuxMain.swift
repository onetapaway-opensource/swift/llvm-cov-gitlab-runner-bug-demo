import XCTest

import llvm_cov_gitlab_runner_bug_demoTests

var tests = [XCTestCaseEntry]()
tests += llvm_cov_gitlab_runner_bug_demoTests.allTests()
XCTMain(tests)
