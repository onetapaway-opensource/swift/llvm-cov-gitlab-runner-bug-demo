import XCTest
@testable import llvm_cov_gitlab_runner_bug_demo

final class llvm_cov_gitlab_runner_bug_demoTests: XCTestCase {
  
  func testHelloWorld() {
    let world = World(name: "World")
    XCTAssertEqual(world.sayHello(), "Hello World")
  }
  
}
