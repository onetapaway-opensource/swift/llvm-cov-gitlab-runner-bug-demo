import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(llvm_cov_gitlab_runner_bug_demoTests.allTests),
    ]
}
#endif
