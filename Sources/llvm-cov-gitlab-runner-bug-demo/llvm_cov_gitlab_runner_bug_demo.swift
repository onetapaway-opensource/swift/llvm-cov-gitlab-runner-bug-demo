struct World {
  
  let name: String
  
  func sayHello() -> String {
    return "Hello \(name)"
  }
  
  func untested() {
    let a = "a"
    let ab = "\(a)b"
    _ = "\(ab)c"
    print("This function is pointless, why'd you call it?")
  }
}
